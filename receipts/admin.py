from django.contrib import admin

# Register your models here.
from receipts.models import ExpenseCategory, Account, Receipt



@admin.register(ExpenseCategory)

class ExpenseCategory(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id",
    )

# Owner shows up with the option to select "mickpreston"
# because it's tied to the "User" Model. "mickpreston" is registered
# as a superuser already.


@admin.register(Account)

class Account(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
        "id",
    )

# Owner shows up with the option to select "mickpreston"
# because it's tied to the "User" Model. "mickpreston" is registered
# as a superuser already.

@admin.register(Receipt)

class Receipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "purchaser",
        "account",
        "id",
    )

# Purchaser and Account show up anyways. I assume because they
# they are tied to the foreign key.

# Purchaser allos you to select select "mickpreston"
# because it's tied to the "User" Model. "mickpreston" is registered
# as a superuser already.

# Account shows up but you cannot select anything yet. Because
# nothing has been added to the database?