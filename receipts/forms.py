from django import forms
from receipts.models import Receipt, ExpenseCategory, Account



class ReceiptForm(forms.ModelForm):
    class Meta:

        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

    # def __init__(self, *args,**kwargs):
    #     user = kwargs.pop("user")
    #     super(ReceiptForm, self).__init__(*args, **kwargs)
    #     self.fields['category'].queryset = Receipt.objects.filter(user=user)


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]