from django.db import models
from django.conf import settings
# imports settings file from django project (expenses)
# Create your models here.

class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,             # This allows us to associate a user to Expense Category. BUT IT TIES TO ANY USER
        related_name = "categories",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,             # This allows us to associate a user to Expense Category. BUT IT TIES TO ANY USER
        related_name="accounts",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name



class Receipt(models.Model):
    vendor=models.CharField(max_length=200)
    total=models.DecimalField(max_digits=10,decimal_places=3)
    tax=models.DecimalField(max_digits=10,decimal_places=3)
    date=models.DateTimeField() #  We want this to be an entry field because you "enter the receipt" into the app, and "enter the time of transaction"

    purchaser= models.ForeignKey(
        settings.AUTH_USER_MODEL,             # This allows us to associate a user to Expense Category. BUT IT TIES TO ANY USER
        related_name="receipts",
        on_delete=models.CASCADE
    )

    category= models.ForeignKey(
        ExpenseCategory,                        # This allows us to associate a Expense category to Receipt
        related_name="receipts",                # This (receipt) is a variable inside expense category
        on_delete=models.CASCADE
    )

    account=models.ForeignKey(
        Account,                       # This allows us to associate an Account to Receipt
        related_name="bob",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.vendor
