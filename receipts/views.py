from django.shortcuts import render, redirect, get_object_or_404
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.

@login_required  # This line requires a user to be logged in to see the homepage
def home_page(request):
    receipts = Receipt.objects.all().filter(purchaser=request.user) # We this from what it was previously. This line filters so that the user of the request can only see their receipts.
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/receipt_list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_categories": categories
    }
    return render(request, "categories/category_list.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, "accounts/account_list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")

    else:
        form = ExpenseCategoryForm()

    context = {
        "expense_form": form,
    }

    return render(request, "categories/create.html", context)



@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")

    else:
        form = AccountForm()

    context = {
        "account_form": form,
    }

    return render(request, "accounts/create.html", context)